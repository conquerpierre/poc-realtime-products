package com.smartsmark.pocrealtime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class ApiRefresh {


    @RequestMapping(value = "api/product/allProducts", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Product>> initRepositoru() {

        ProductRepo productrepo = new ProductRepo();

        return new ResponseEntity<List<Product>>(productrepo.getProducts(), HttpStatus.MULTI_STATUS.OK);

    }

    @RequestMapping(value = "api/product/refresh/product", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Product> refreshProductQuantity(@RequestBody Product productReq) {
        ProductRepo  productrepo= new ProductRepo();
        String productName = productReq.getNom();
        int quantity = productReq.getQuantity();


        boolean found = false;
        Product productFound = null;
        for (Product product : productrepo.getProducts()) {
            if(product.getNom().equals(productName)) {
                found = true;
                product.setQuantity(quantity);
                productFound = product;
            }
        }
        if(found)
            return new ResponseEntity<Product>(productFound, HttpStatus.MULTI_STATUS.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }







    }


