package com.smartsmark.pocrealtime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocrealtimeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocrealtimeApplication.class, args);
    }

}
