package com.smartsmark.pocrealtime;

import java.util.UUID;

public class Product {

    private UUID uid;
    private int quantity;
    private String nom;

    public Product(UUID uid, int quantity, String nom) {
        this.uid = uid;
        this.quantity = quantity;
        this.nom = nom;
    }

    public UUID getUid() {
        return uid;
    }

    public void setUid(UUID uid) {
        this.uid = uid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

}
