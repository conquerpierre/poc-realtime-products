package com.smartsmark.pocrealtime;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProductRepo {

    private ArrayList<Product> products;

    public ProductRepo() {
        products = new ArrayList<Product>();
        Product p1 = new Product(UUID.randomUUID(), 2, "produit1");
        Product p2 = new Product((UUID.randomUUID()), 4, "produit2");
        Product p3 = new Product((UUID.randomUUID()), 6, "produit3");
        products.add(p1);
        products.add(p2);
        products.add(p3);

    }

    public ArrayList<Product> getProducts(){
        return products;
    }
}
